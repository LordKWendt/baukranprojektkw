﻿using System.Windows.Forms;

namespace BauKranResources.MyButtons
{
	public abstract class AMyButton : Button
	{
		public AMyButton() {
			Size = new System.Drawing.Size(150, 50);
			UseVisualStyleBackColor = true;
		}
	}
}
