﻿using System.Drawing;
using System.Windows.Forms;
using BauKranResources.MyPanels.InterfacePanels;

namespace BauKranResources.MyPanels.AbstractPanels
{
	public abstract class ACranePanel : Panel, ICranePanel
	{
		#region Getter
		public int GetXleft() => Location.X;
		public int GetYup() => Location.Y;
		public int GetWidth() => Size.Width;
		public int GetHeight() => Size.Height;
		public int GetXright() => GetXleft() + GetWidth();
		public int GetYdown() => GetYup() + GetHeight();
		#endregion

		#region Setter
		private void SetLocation(int x, int y) => Location = new Point(x, y);
		private void SetSize(int x, int y) => Size = new Size(x, y);
		#endregion

		#region Konstruktor
		private readonly int clientSizeX = 1904;
		private readonly int clientSizeY = 1041;
		public ACranePanel()
		{
			LimitLeft = 0;
			LimitRight = clientSizeX;
			LimitUp = 0;
			LimitDown = clientSizeY;
		}
		#endregion

		#region Move Left/Right 
		public int LimitLeft { get; set; }
		public int LimitRight { get; set; }

		private int CheckMyXLimitLeft(int result) => result < LimitLeft ? LimitLeft : result;
		private int CheckMyXLimitRight(int result) => result > LimitRight ? LimitRight : result;

		public void MoveLeft(int range) => SetLocation(CheckMyXLimitLeft(GetXleft() - range), GetYup());
		public void MoveRight(int range) => SetLocation(CheckMyXLimitRight(GetXright() + range), GetYup());
		#endregion

		#region Grow Left
		public void MoveLeftAndScale(int range)
		{
			MoveLeft(range);
			Wider(range);
		}
		public void MoveRightAndScale(int range)
		{
			MoveRight(range);
			Narrower(range);
		}

		private void Wider(int range) => Size = new Size(Size.Width + range, Size.Height);
		private void Narrower(int range) => Size = new Size(Size.Width - range, Size.Height);
		#endregion


		public void MoveUpAndScale(int range)
		{
			MoveUp(range);
			Higher(range);
		}
		public void MoveDownAndScale(int range)
		{
			MoveDown(range);
			Lower(range);
		}

		public void MoveUp(int range) => Location = new Point(GetXleft(), LimitCheckYup(GetYup() - range));
		public void MoveDown(int range) => Location = new Point(GetXleft(), LimitCheckYup(GetYup() + range));

		public int LimitUp { get; set; }
		public int LimitDown { get; set; }


		private int LimitCheckYup(int result)
		{
			return result < LimitUp ? LimitUp : result;
		}
		private int LimitCheckYDown(int result)
		{
			return result + GetHeight() > LimitDown ? LimitDown - GetHeight() : result;
		}

		private void Higher(int range) => Size = new Size(GetWidth(), LimitCheckYup(GetHeight() + range));
		private void Lower(int range) => Size = new Size(GetWidth(), LimitCheckYDown(GetHeight() - range));
	}
}
