﻿namespace BauKranResources.MyPanels.InterfacePanels
{
	interface ICranePanel
	{
		void MoveLeft(int range);
		void MoveRight(int range);
		void MoveUp(int range);
		void MoveDown(int range);

		int LimitLeft { get; set; }
		int LimitRight { get; set; }

		int GetXleft();
		int GetYup();
		int GetWidth();
		int GetHeight();
		int GetXright();
		int GetYdown();
	}
}
