﻿using BauKranControler;
using BauKranView;
using System;
using System.Windows.Forms;


namespace BauKranStartPunkt
{
	static class BauKranStartPunkt
	{
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			BauKranForm bauKranFormView = new BauKranForm();
			Application.Run(bauKranFormView);
			//Application.Run(bauKranFormView(new BauKranControler(bauKranFormView)));

			//new BauKranControler(bauKranFormView);
		}
	}
}
