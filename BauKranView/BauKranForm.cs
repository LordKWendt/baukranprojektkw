﻿//using BauKranControler;
using BauKranResources.MyPanels.AbstractPanels;
using System;
using System.Windows.Forms;

namespace BauKranView
{
	public partial class BauKranForm : Form
	{
		//BauKranControler.BauKranControler controler;
		//public BauKranForm(BauKranControler.BauKranControler controler)
		//{
		//	InitializeComponent();
		//	//controler = new BauKranControler(this);
		//}
		public BauKranForm()
		{
			InitializeComponent();
			//controler = new BauKranControler(this);
			flowLayoutPanelControl.BringToFront();
			craneTrackPanel.BringToFront();
			craneBasePanel.BringToFront();
			cranePillarPanel.BringToFront();
			craneFlatPanel.BringToFront();
			craneHookTrackPanel.BringToFront();
			craneHookPanel.BringToFront();


			craneBasePanel.LimitRight = craneBasePanel.GetXright();
			craneBasePanel.LimitLeft = craneTrackPanel.GetXleft();
			cranePillarPanel.LimitRight = cranePillarPanel.GetXright();
			craneFlatPanel.LimitRight = craneFlatPanel.GetXleft();
			craneHookTrackPanel.LimitRight = craneHookTrackPanel.GetXright();
			craneHookPanel.LimitRight = craneHookTrackPanel.GetXright();




		}


		public Panel GetCraneTrackPanel()
		{
			return craneTrackPanel;
		}
		public ACranePanel GetCraneBasePanel()
		{
			return craneBasePanel;
		}
		public ACranePanel GetCranePillarPanel()
		{
			return cranePillarPanel;
		}
		public ACranePanel GetCraneFlatPanel()
		{
			return craneFlatPanel;
		}
		public ACranePanel GetCraneHookTrackPanel()
		{
			return craneHookTrackPanel;
		}
		public ACranePanel GetCraneHookPanel()
		{
			return craneHookPanel;
		}


		private void buttonBaseLeft_Click(object sender, EventArgs e)
		{
			craneBasePanel.MoveLeft(10);
			cranePillarPanel.MoveLeft(10);
			craneFlatPanel.MoveLeft(10);
			craneHookTrackPanel.MoveLeft(10);
			craneHookPanel.MoveLeft(10);
		}

		private void buttonBaseRight_Click(object sender, EventArgs e)
		{
			craneBasePanel.MoveRight(10);
			cranePillarPanel.MoveRight(10);
			craneFlatPanel.MoveRight(10);
			craneHookTrackPanel.MoveRight(10);
			craneHookPanel.MoveRight(10);
		}

		private void pillarUp_Click(object sender, EventArgs e)
		{
			cranePillarPanel.MoveUpAndScale(10);
			craneFlatPanel.MoveUp(10);
			craneHookTrackPanel.MoveUp(10);
			craneHookPanel.MoveUp(10);

		}
		private void pillarDown_Click(object sender, EventArgs e)
		{
			cranePillarPanel.MoveDownAndScale(10);
			craneFlatPanel.MoveDown(10);
			craneHookTrackPanel.MoveDown(10);
			craneHookPanel.MoveDown(10);
		}

		private void hookDown_Click(object sender, EventArgs e)
		{

		}

		private void hookForward_Click(object sender, EventArgs e)
		{

		}

		private void hookBack_Click(object sender, EventArgs e)
		{

		}

		private void ArmBack_Click(object sender, EventArgs e)
		{

		}

		private void ArmForwards_Click(object sender, EventArgs e)
		{

		}

		private void hookUp_Click(object sender, EventArgs e)
		{

		}

	
	}
}
