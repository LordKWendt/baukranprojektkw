﻿using BauKranResources.MyPanels;
using BauKranResources.MyButtons;
using System.Windows.Forms;

namespace BauKranView
{
	partial class BauKranForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BauKranForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.craneBasePanel = new BauKranResources.MyPanels.CraneBasePanel();
			this.flowLayoutPanelControl = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonHookDown = new BauKranResources.MyButtons.CraneButton();
			this.buttonHookUp = new BauKranResources.MyButtons.CraneButton();
			this.buttonHookForward = new BauKranResources.MyButtons.CraneButton();
			this.buttonHookBack = new BauKranResources.MyButtons.CraneButton();
			this.buttonArmForwards = new BauKranResources.MyButtons.CraneButton();
			this.buttonArmBack = new BauKranResources.MyButtons.CraneButton();
			this.buttonPillarUp = new BauKranResources.MyButtons.CraneButton();
			this.buttonPillarDown = new BauKranResources.MyButtons.CraneButton();
			this.buttonBaseLeft = new BauKranResources.MyButtons.CraneButton();
			this.buttonBaseRight = new BauKranResources.MyButtons.CraneButton();
			this.craneTrackPanel = new BauKranResources.MyPanels.CraneTrackPanel();
			this.cranePillarPanel = new BauKranResources.MyPanels.CranePillarPanel();
			this.craneFlatPanel = new BauKranResources.MyPanels.CraneFlatPanel();
			this.craneHookTrackPanel = new BauKranResources.MyPanels.CraneHookTrackPanel();
			this.craneHookPanel = new BauKranResources.MyPanels.CraneHookPanel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.flowLayoutPanelControl.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Green;
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1960, 750);
			this.panel1.TabIndex = 0;
			// 
			// craneBasePanel
			// 
			this.craneBasePanel.BackColor = System.Drawing.Color.Yellow;
			this.craneBasePanel.LimitDown = 1041;
			this.craneBasePanel.LimitLeft = 500;
			this.craneBasePanel.LimitRight = 1500;
			this.craneBasePanel.LimitUp = 0;
			this.craneBasePanel.Location = new System.Drawing.Point(1300, 950);
			this.craneBasePanel.Margin = new System.Windows.Forms.Padding(0);
			this.craneBasePanel.Name = "craneBasePanel";
			this.craneBasePanel.Size = new System.Drawing.Size(200, 50);
			this.craneBasePanel.TabIndex = 1;
			// 
			// flowLayoutPanelControl
			// 
			this.flowLayoutPanelControl.BackColor = System.Drawing.Color.White;
			this.flowLayoutPanelControl.Controls.Add(this.buttonHookDown);
			this.flowLayoutPanelControl.Controls.Add(this.buttonHookUp);
			this.flowLayoutPanelControl.Controls.Add(this.buttonHookForward);
			this.flowLayoutPanelControl.Controls.Add(this.buttonHookBack);
			this.flowLayoutPanelControl.Controls.Add(this.buttonArmForwards);
			this.flowLayoutPanelControl.Controls.Add(this.buttonArmBack);
			this.flowLayoutPanelControl.Controls.Add(this.buttonPillarUp);
			this.flowLayoutPanelControl.Controls.Add(this.buttonPillarDown);
			this.flowLayoutPanelControl.Controls.Add(this.buttonBaseLeft);
			this.flowLayoutPanelControl.Controls.Add(this.buttonBaseRight);
			this.flowLayoutPanelControl.Location = new System.Drawing.Point(1628, 27);
			this.flowLayoutPanelControl.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanelControl.Name = "flowLayoutPanelControl";
			this.flowLayoutPanelControl.Size = new System.Drawing.Size(200, 250);
			this.flowLayoutPanelControl.TabIndex = 2;
			// 
			// buttonHookDown
			// 
			this.buttonHookDown.Location = new System.Drawing.Point(0, 0);
			this.buttonHookDown.Margin = new System.Windows.Forms.Padding(0);
			this.buttonHookDown.Name = "buttonHookDown";
			this.buttonHookDown.Size = new System.Drawing.Size(100, 50);
			this.buttonHookDown.TabIndex = 0;
			this.buttonHookDown.Text = "Haken runter";
			this.buttonHookDown.UseVisualStyleBackColor = true;
			this.buttonHookDown.Click += new System.EventHandler(this.hookDown_Click);
			// 
			// buttonHookUp
			// 
			this.buttonHookUp.Location = new System.Drawing.Point(100, 0);
			this.buttonHookUp.Margin = new System.Windows.Forms.Padding(0);
			this.buttonHookUp.Name = "buttonHookUp";
			this.buttonHookUp.Size = new System.Drawing.Size(100, 50);
			this.buttonHookUp.TabIndex = 0;
			this.buttonHookUp.Text = "Haken hoch";
			this.buttonHookUp.UseVisualStyleBackColor = true;
			this.buttonHookUp.Click += new System.EventHandler(this.hookUp_Click);
			// 
			// buttonHookForward
			// 
			this.buttonHookForward.Location = new System.Drawing.Point(0, 50);
			this.buttonHookForward.Margin = new System.Windows.Forms.Padding(0);
			this.buttonHookForward.Name = "buttonHookForward";
			this.buttonHookForward.Size = new System.Drawing.Size(100, 50);
			this.buttonHookForward.TabIndex = 4;
			this.buttonHookForward.Text = "Haken vor";
			this.buttonHookForward.UseVisualStyleBackColor = true;
			this.buttonHookForward.Click += new System.EventHandler(this.hookForward_Click);
			// 
			// buttonHookBack
			// 
			this.buttonHookBack.Location = new System.Drawing.Point(100, 50);
			this.buttonHookBack.Margin = new System.Windows.Forms.Padding(0);
			this.buttonHookBack.Name = "buttonHookBack";
			this.buttonHookBack.Size = new System.Drawing.Size(100, 50);
			this.buttonHookBack.TabIndex = 0;
			this.buttonHookBack.Text = "Haken zurück";
			this.buttonHookBack.UseVisualStyleBackColor = true;
			this.buttonHookBack.Click += new System.EventHandler(this.hookBack_Click);
			// 
			// buttonArmForwards
			// 
			this.buttonArmForwards.Location = new System.Drawing.Point(0, 100);
			this.buttonArmForwards.Margin = new System.Windows.Forms.Padding(0);
			this.buttonArmForwards.Name = "buttonArmForwards";
			this.buttonArmForwards.Size = new System.Drawing.Size(100, 50);
			this.buttonArmForwards.TabIndex = 6;
			this.buttonArmForwards.Text = "Arm vor";
			this.buttonArmForwards.UseVisualStyleBackColor = true;
			this.buttonArmForwards.Click += new System.EventHandler(this.ArmForwards_Click);
			// 
			// buttonArmBack
			// 
			this.buttonArmBack.Location = new System.Drawing.Point(100, 100);
			this.buttonArmBack.Margin = new System.Windows.Forms.Padding(0);
			this.buttonArmBack.Name = "buttonArmBack";
			this.buttonArmBack.Size = new System.Drawing.Size(100, 50);
			this.buttonArmBack.TabIndex = 7;
			this.buttonArmBack.Text = "Arm zurück";
			this.buttonArmBack.UseVisualStyleBackColor = true;
			this.buttonArmBack.Click += new System.EventHandler(this.ArmBack_Click);
			// 
			// buttonPillarUp
			// 
			this.buttonPillarUp.Location = new System.Drawing.Point(0, 150);
			this.buttonPillarUp.Margin = new System.Windows.Forms.Padding(0);
			this.buttonPillarUp.Name = "buttonPillarUp";
			this.buttonPillarUp.Size = new System.Drawing.Size(100, 50);
			this.buttonPillarUp.TabIndex = 9;
			this.buttonPillarUp.Text = "Kran hoch";
			this.buttonPillarUp.UseVisualStyleBackColor = true;
			this.buttonPillarUp.Click += new System.EventHandler(this.pillarUp_Click);
			// 
			// buttonPillarDown
			// 
			this.buttonPillarDown.Location = new System.Drawing.Point(100, 150);
			this.buttonPillarDown.Margin = new System.Windows.Forms.Padding(0);
			this.buttonPillarDown.Name = "buttonPillarDown";
			this.buttonPillarDown.Size = new System.Drawing.Size(100, 50);
			this.buttonPillarDown.TabIndex = 10;
			this.buttonPillarDown.Text = "Kran runter";
			this.buttonPillarDown.UseVisualStyleBackColor = true;
			this.buttonPillarDown.Click += new System.EventHandler(this.pillarDown_Click);
			// 
			// buttonBaseLeft
			// 
			this.buttonBaseLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.buttonBaseLeft.Location = new System.Drawing.Point(0, 200);
			this.buttonBaseLeft.Margin = new System.Windows.Forms.Padding(0);
			this.buttonBaseLeft.Name = "buttonBaseLeft";
			this.buttonBaseLeft.Size = new System.Drawing.Size(100, 50);
			this.buttonBaseLeft.TabIndex = 8;
			this.buttonBaseLeft.Text = "Kran nach links";
			this.buttonBaseLeft.UseVisualStyleBackColor = false;
			this.buttonBaseLeft.Click += new System.EventHandler(this.buttonBaseLeft_Click);
			// 
			// buttonBaseRight
			// 
			this.buttonBaseRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.buttonBaseRight.Location = new System.Drawing.Point(100, 200);
			this.buttonBaseRight.Margin = new System.Windows.Forms.Padding(0);
			this.buttonBaseRight.Name = "buttonBaseRight";
			this.buttonBaseRight.Size = new System.Drawing.Size(100, 50);
			this.buttonBaseRight.TabIndex = 11;
			this.buttonBaseRight.Text = "Kran nach rechts";
			this.buttonBaseRight.UseVisualStyleBackColor = false;
			this.buttonBaseRight.Click += new System.EventHandler(this.buttonBaseRight_Click);
			// 
			// craneTrackPanel
			// 
			this.craneTrackPanel.BackColor = System.Drawing.Color.Black;
			this.craneTrackPanel.LimitDown = 1041;
			this.craneTrackPanel.LimitLeft = 0;
			this.craneTrackPanel.LimitRight = 1904;
			this.craneTrackPanel.LimitUp = 0;
			this.craneTrackPanel.Location = new System.Drawing.Point(500, 1000);
			this.craneTrackPanel.Margin = new System.Windows.Forms.Padding(0);
			this.craneTrackPanel.Name = "craneTrackPanel";
			this.craneTrackPanel.Size = new System.Drawing.Size(1000, 20);
			this.craneTrackPanel.TabIndex = 0;
			// 
			// cranePillarPanel
			// 
			this.cranePillarPanel.BackColor = System.Drawing.Color.Yellow;
			this.cranePillarPanel.LimitDown = 941;
			this.cranePillarPanel.LimitLeft = 0;
			this.cranePillarPanel.LimitRight = 1704;
			this.cranePillarPanel.LimitUp = 0;
			this.cranePillarPanel.Location = new System.Drawing.Point(1370, 470);
			this.cranePillarPanel.Margin = new System.Windows.Forms.Padding(0);
			this.cranePillarPanel.Name = "cranePillarPanel";
			this.cranePillarPanel.Size = new System.Drawing.Size(60, 480);
			this.cranePillarPanel.TabIndex = 3;
			// 
			// craneFlatPanel
			// 
			this.craneFlatPanel.BackColor = System.Drawing.Color.Yellow;
			this.craneFlatPanel.LimitDown = 1041;
			this.craneFlatPanel.LimitLeft = 0;
			this.craneFlatPanel.LimitRight = 1704;
			this.craneFlatPanel.LimitUp = 0;
			this.craneFlatPanel.Location = new System.Drawing.Point(1070, 530);
			this.craneFlatPanel.Name = "craneFlatPanel";
			this.craneFlatPanel.Size = new System.Drawing.Size(480, 60);
			this.craneFlatPanel.TabIndex = 0;
			// 
			// craneHookTrackPanel
			// 
			this.craneHookTrackPanel.BackColor = System.Drawing.Color.Black;
			this.craneHookTrackPanel.LimitDown = 1041;
			this.craneHookTrackPanel.LimitLeft = 0;
			this.craneHookTrackPanel.LimitRight = 1704;
			this.craneHookTrackPanel.LimitUp = 0;
			this.craneHookTrackPanel.Location = new System.Drawing.Point(1130, 585);
			this.craneHookTrackPanel.Margin = new System.Windows.Forms.Padding(0);
			this.craneHookTrackPanel.Name = "craneHookTrackPanel";
			this.craneHookTrackPanel.Size = new System.Drawing.Size(120, 11);
			this.craneHookTrackPanel.TabIndex = 4;
			// 
			// craneHookPanel
			// 
			this.craneHookPanel.BackColor = System.Drawing.Color.Yellow;
			this.craneHookPanel.LimitDown = 1041;
			this.craneHookPanel.LimitLeft = 0;
			this.craneHookPanel.LimitRight = 1704;
			this.craneHookPanel.LimitUp = 0;
			this.craneHookPanel.Location = new System.Drawing.Point(1185, 596);
			this.craneHookPanel.Margin = new System.Windows.Forms.Padding(0);
			this.craneHookPanel.Name = "craneHookPanel";
			this.craneHookPanel.Size = new System.Drawing.Size(10, 30);
			this.craneHookPanel.TabIndex = 5;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.Blue;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Margin = new System.Windows.Forms.Padding(0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(597, 339);
			this.panel2.TabIndex = 0;
			// 
			// BauKranForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.ClientSize = new System.Drawing.Size(1904, 1041);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.craneHookPanel);
			this.Controls.Add(this.craneHookTrackPanel);
			this.Controls.Add(this.craneFlatPanel);
			this.Controls.Add(this.cranePillarPanel);
			this.Controls.Add(this.craneTrackPanel);
			this.Controls.Add(this.flowLayoutPanelControl);
			this.Controls.Add(this.craneBasePanel);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "BauKranForm";
			this.Text = "BauKran";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.panel1.ResumeLayout(false);
			this.flowLayoutPanelControl.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private Panel panel1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelControl;
		private CraneBasePanel craneBasePanel;
		private CraneTrackPanel craneTrackPanel;
		private CraneHookTrackPanel craneHookTrackPanel;
		private CranePillarPanel cranePillarPanel;
		private CraneFlatPanel craneFlatPanel;
		private CraneHookPanel craneHookPanel;
		
		private CraneButton buttonHookDown;
		private CraneButton buttonHookUp;
		private CraneButton buttonHookForward;
		private CraneButton buttonHookBack;
		private CraneButton buttonArmForwards;
		private CraneButton buttonArmBack;
		private CraneButton buttonPillarUp;
		private CraneButton buttonPillarDown;
		private CraneButton buttonBaseLeft;
		private CraneButton buttonBaseRight;
		private Panel panel2;
	}
}

